package br.com.crud.service;

import br.com.crud.model.Pessoa;
import br.com.crud.repository.PessoaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PessoaService{
  private final PessoaRepository repository;

  @Autowired
  public PessoaService(PessoaRepository repository){
    this.repository=repository;
  }

  public Pessoa save(Pessoa pessoa){
    return repository.save(pessoa);
  }

  public List<Pessoa> findAll(){
    return repository.findAll();
  }

  public Optional<Pessoa> findById(Long id){
    return repository.findById(id);
  }

  public Pessoa update(Pessoa pessoa){
    return repository.save(pessoa);
  }

  public void delete(Long id){
    repository.deleteById(id);
  }
}
