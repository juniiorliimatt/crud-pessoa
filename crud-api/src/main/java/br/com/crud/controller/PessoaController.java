package br.com.crud.controller;

import br.com.crud.model.Pessoa;
import br.com.crud.service.PessoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/pessoa")
public class PessoaController{
  private final PessoaService service;

  @Autowired
  public PessoaController(PessoaService service){
    this.service=service;
  }

  @GetMapping("/all")
  public ResponseEntity<List<Pessoa>> findAll(){
    List<Pessoa> pessoas=service.findAll();
    return new ResponseEntity<>(pessoas, HttpStatus.OK);
  }

  @GetMapping("/find/{id}")
  public ResponseEntity<Pessoa> findById(@PathVariable("id") Long id){
    Optional<Pessoa> pessoa=service.findById(id);
    return pessoa
        .map(value->new ResponseEntity<>(value, HttpStatus.OK))
        .orElseGet(()->new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @PostMapping("/add")
  public ResponseEntity<Pessoa> save(@RequestBody Pessoa pessoa){
    Pessoa newPessoa=service.save(pessoa);
    return new ResponseEntity<>(newPessoa, HttpStatus.CREATED);
  }

  @PutMapping("/update")
  public ResponseEntity<Pessoa> update(@RequestBody Pessoa pessoa){
    Pessoa updatePessoa=service.update(pessoa);
    return new ResponseEntity<>(updatePessoa, HttpStatus.OK);
  }

  @DeleteMapping("/delete/{id}")
  public ResponseEntity<Pessoa> delete(@PathVariable("id") Long id){
    service.delete(id);
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
