package br.com.crud.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="pessoas")
public class Pessoa implements Serializable{

  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  private Long id;
  private String nome;
  private Integer idade;

  public Pessoa(){
  }

  public Pessoa(Long id, String nome, Integer idade){
    this.id=id;
    this.nome=nome;
    this.idade=idade;
  }

  public Long getId(){
    return id;
  }

  public String getNome(){
    return nome;
  }

  public Integer getIdade(){
    return idade;
  }
}
