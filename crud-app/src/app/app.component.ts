import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { Pessoa } from './pessoa';
import { PessoaService } from './pessoa.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public pessoas: Pessoa[] = [];
  public pessoa?: Pessoa;
  public editarPessoa?: Pessoa;
  public deletarPessoa?: Pessoa;

  constructor(private service: PessoaService) { }

  ngOnInit() {
    this.getPessoas();
  }

  public getPessoas(): void {
    this.service.getPessoas().subscribe(
      (pessoasFromApi: Pessoa[]) => {
        this.pessoas = pessoasFromApi;
      },
      (error: HttpErrorResponse) => {
        alert(error.message)
      }
    )
  }

  public savePessoa(form: NgForm): void {
    document.getElementById('close-form')?.click();
    this.service.addPessoa(form.value).subscribe(
      () => {
        this.getPessoas();
        form.reset();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
  }

  public updatePessoa(pessoa: Pessoa): void {
    this.service.updatePessoa(pessoa).subscribe(
      () => {
        this.getPessoas();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
  }

  public deletePessoa(id: number): void {
    this.service.deletarPessoa(id).subscribe(
      () => {
        this.getPessoas();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
  }

  public searchPessoa(key: string): void {
    const results: Pessoa[] = [];
    for (const pessoa of this.pessoas) {
      if (pessoa.nome.toLocaleLowerCase().indexOf(key.toLocaleLowerCase()) !== -1) {
        results.push(pessoa);
      }
    }
    this.pessoas = results;
    if (results.length === 0 || !key) {
      this.getPessoas();
    }
  }

  public openModal(modo: string, pessoa?: Pessoa): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');

    if (modo === 'adicionar') {
      button.setAttribute('data-target', '#addEmployeeModal');
    }
    if (modo === 'editar') {
      this.editarPessoa = pessoa;
      button.setAttribute('data-target', '#updateEmployeeModal');
    }
    if (modo === 'deletar') {
      this.deletarPessoa = pessoa;
      button.setAttribute('data-target', '#deleteEmployeeModal');
    }
    container?.appendChild(button);
    button.click();
  }
}
