import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Pessoa } from './pessoa';

@Injectable({
  providedIn: 'root'
})
export class PessoaService {
  private apiUrl = 'http://localhost:8080/pessoa'

  constructor(private http: HttpClient) { }

  public getPessoas(): Observable<Pessoa[]> {
    return this.http.get<Pessoa[]>(`${this.apiUrl}/all`);
  }

  public addPessoa(pessoa: Pessoa): Observable<Pessoa> {
    return this.http.post<Pessoa>(`${this.apiUrl}/add`, pessoa);
  }

  public updatePessoa(pessoa: Pessoa): Observable<Pessoa> {
    return this.http.put<Pessoa>(`${this.apiUrl}/update`, pessoa);
  }

  public deletarPessoa(id: number): Observable<Pessoa> {
    return this.http.delete<Pessoa>(`${this.apiUrl}/delete/${id}`)
  }
}
